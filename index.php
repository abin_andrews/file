<?php
$dir    = '/var/www/';
$files = scandir($dir);

$listing = array();

foreach($files as $file) {

    $isDir = false;
    $isLink = false;

    $details = stat($dir.$file);

    $fileSize = 0;

    if(is_dir($dir.$file)) {
       $isDir = true;
    }



    $path_parts = pathinfo($dir.$file);

    $listing[] = [
        'file' => $file,
        'isDir' => $isDir,
        'details' => $details,
        'path_parts' => $path_parts,
        'permission' => fileperms($dir.$file )
    ];
}

echo "<pre>";
print_r($listing);


?>